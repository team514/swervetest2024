// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class DriveUtil extends SubsystemBase {
  public CANSparkMax module1_angle, module1_drive;
  public CANSparkMax module2_angle, module2_drive;
  public CANSparkMax module3_angle, module3_drive;
  public CANSparkMax module4_angle, module4_drive;

  //public CANSparkMax[] AllSparks = {}


  /** Creates a new ExampleSubsystem. */
  public DriveUtil() {
    module1_angle = new CANSparkMax(Constants.swerveModule1_angle, MotorType.kBrushless);
    module1_drive = new CANSparkMax(Constants.swerveModule1_drive, MotorType.kBrushless);
    module2_angle = new CANSparkMax(Constants.swerveModule2_angle, MotorType.kBrushless);
    module2_drive = new CANSparkMax(Constants.swerveModule2_drive, MotorType.kBrushless);
    module3_angle = new CANSparkMax(Constants.swerveModule3_angle, MotorType.kBrushless);
    module3_drive = new CANSparkMax(Constants.swerveModule3_drive, MotorType.kBrushless);
    module4_angle = new CANSparkMax(Constants.swerveModule4_angle, MotorType.kBrushless);
    module4_drive = new CANSparkMax(Constants.swerveModule4_drive, MotorType.kBrushless);

    //AllSparks.
  }

  /**
   * Example command factory method.
   *
   * @return a command
   */
  public Command exampleMethodCommand() {
    // Inline construction of command goes here.
    // Subsystem::RunOnce implicitly requires `this` subsystem.
    return runOnce(
        () -> {
          /* one-time action goes here */
        });
  }

  /**
   * An example method querying a boolean state of the subsystem (for example, a digital sensor).
   *
   * @return value of some boolean subsystem state, such as a digital sensor.
   */
  public boolean exampleCondition() {
    // Query some boolean state, such as a digital sensor.
    return false;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}
