// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

  public static final double autoTestSpeed = 0.2;

  public static final int swerveModule1_angle = 1;
  public static final int swerveModule1_drive = 2;
  public static final int swerveModule1_encoder = 9;

  public static final int swerveModule2_angle = 3;
  public static final int swerveModule2_drive = 4;
  public static final int swerveModule2_encoder = 10;

  public static final int swerveModule3_angle = 5;
  public static final int swerveModule3_drive = 6;
  public static final int swerveModule3_encoder = 11;

  public static final int swerveModule4_angle = 7;
  public static final int swerveModule4_drive = 8;
  public static final int swerveModule4_encoder = 12;

  public static final int pigeon2 = 13;

  public static class OperatorConstants {
    public static final int kDriverControllerPort = 0;
  }
}
