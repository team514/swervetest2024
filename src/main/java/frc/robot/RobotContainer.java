// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import frc.robot.Constants.OperatorConstants;
import frc.robot.commands.Autos;
import frc.robot.commands.ExampleCommand;
import frc.robot.subsystems.DriveUtil;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.Trigger;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private final DriveUtil driveUtil = new DriveUtil();
  private JoystickButton module1_angle, module1_drive, module2_angle, module2_drive, module3_angle, module3_drive, module4_angle, module4_drive;

  // Replace with CommandPS4Controller or CommandJoystick if needed
  private final CommandXboxController m_driverController =
      new CommandXboxController(OperatorConstants.kDriverControllerPort);

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    // Configure the trigger bindings
    configureBindings();
  }

  /**
   * Use this method to define your trigger->command mappings. Triggers can be created via the
   * {@link Trigger#Trigger(java.util.function.BooleanSupplier)} constructor with an arbitrary
   * predicate, or via the named factories in {@link
   * edu.wpi.first.wpilibj2.command.button.CommandGenericHID}'s subclasses for {@link
   * CommandXboxController Xbox}/{@link edu.wpi.first.wpilibj2.command.button.CommandPS4Controller
   * PS4} controllers or {@link edu.wpi.first.wpilibj2.command.button.CommandJoystick Flight
   * joysticks}.
   */
  private void configureBindings() {
    module1_angle = new JoystickButton(m_driverController.getHID(), 1);
    module1_drive = new JoystickButton(m_driverController.getHID(), 2);
    module2_angle = new JoystickButton(m_driverController.getHID(), 3);
    module2_drive = new JoystickButton(m_driverController.getHID(), 4);
    module3_angle = new JoystickButton(m_driverController.getHID(), 5);
    module3_drive = new JoystickButton(m_driverController.getHID(), 6);
    module4_angle = new JoystickButton(m_driverController.getHID(), 7);
    module4_drive = new JoystickButton(m_driverController.getHID(), 8);

    module1_angle.whileTrue(new RunCommand(()->{
      System.out.println("1A");
      driveUtil.module1_angle.set(Constants.autoTestSpeed);
    }, driveUtil));

    module2_angle.whileTrue(new RunCommand(()->{
      System.out.println("2A");
      driveUtil.module2_angle.set(Constants.autoTestSpeed);
    }, driveUtil));

    module3_angle.whileTrue(new RunCommand(()->{
      System.out.println("3A");
      driveUtil.module3_angle.set(Constants.autoTestSpeed);
    }, driveUtil));

    module4_angle.whileTrue(new RunCommand(()->{
      System.out.println("4A");
      driveUtil.module4_angle.set(Constants.autoTestSpeed);
    }, driveUtil));

    module1_drive.whileTrue(new RunCommand(()->{
      System.out.println("1D");
      driveUtil.module1_drive.set(Constants.autoTestSpeed);
    }, driveUtil));

    module2_drive.whileTrue(new RunCommand(()->{
      System.out.println("2D");
      driveUtil.module2_drive.set(Constants.autoTestSpeed);
    }, driveUtil));

    module3_drive.whileTrue(new RunCommand(()->{
      System.out.println("3D");
      driveUtil.module3_drive.set(Constants.autoTestSpeed);
    }, driveUtil));

    module4_drive.whileTrue(new RunCommand(()->{
      System.out.println("4D");
      driveUtil.module4_drive.set(Constants.autoTestSpeed);
    }, driveUtil));

    driveUtil.setDefaultCommand(new RunCommand(()->{
      driveUtil.module1_angle.set(0);
      driveUtil.module1_drive.set(0);
      driveUtil.module2_angle.set(0);
      driveUtil.module2_drive.set(0);
      driveUtil.module3_angle.set(0);
      driveUtil.module3_drive.set(0);
      driveUtil.module4_angle.set(0);
      driveUtil.module4_drive.set(0);
    }, driveUtil));
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An example command will be run in autonomous
    return Autos.exampleAuto(driveUtil);
  }
}
